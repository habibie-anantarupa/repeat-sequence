using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RepeatSequence.UI
{
    public class MenuController : MonoBehaviour
    {
        public void Show()
        {
            gameObject.SetActive(true);
            OnShow();
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }

        protected virtual void OnShow()
        {

        }
    }
}
