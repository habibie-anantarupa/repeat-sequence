using System;
using UnityEngine.UI;
using UnityEngine;

namespace RepeatSequence.UI
{
    public class GameOverPopupController : PopupController
    {
        [SerializeField] private Button playAgainButton, quitButton;

        public event Action PlayAgainPressed, QuitPressed;
        private void Awake() 
        {
            playAgainButton.onClick.AddListener(OnPlayAgainPressed);
            quitButton.onClick.AddListener(OnQuitPressed);
        }

        private void OnDestroy() 
        {
            playAgainButton.onClick.RemoveAllListeners();
            quitButton.onClick.RemoveAllListeners();
        }

        private void OnPlayAgainPressed()
        {
            Hide();
            PlayAgainPressed?.Invoke();
        }

        private void OnQuitPressed()
        {
            Hide();
            QuitPressed?.Invoke();
        }
    }
}
