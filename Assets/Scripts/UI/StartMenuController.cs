using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

namespace RepeatSequence.UI
{
    public class StartMenuController : MenuController
    {
        [SerializeField] private Button playButton, quitButton, normalButton, hardButton;
        [SerializeField] private Transform mainButtonsParent, modeButtonsParent;

        public event Action<bool> PlayModePressed;
        private void Awake() 
        {
            playButton.onClick.AddListener(Play);
            quitButton.onClick.AddListener(Quit);
            normalButton.onClick.AddListener(() => OnPlayModePressed(false));
            hardButton.onClick.AddListener(() => OnPlayModePressed(true));
        }

        private void OnDestroy() 
        {
            playButton.onClick.RemoveAllListeners();
            quitButton.onClick.RemoveAllListeners();
            normalButton.onClick.RemoveAllListeners();
            hardButton.onClick.RemoveAllListeners();
        }

        protected override void OnShow()
        {
            base.OnShow();
            mainButtonsParent.gameObject.SetActive(true);
            modeButtonsParent.gameObject.SetActive(false);
        }

        private void OnPlayModePressed(bool isHardMode)
        {
            Hide();
            PlayModePressed?.Invoke(isHardMode);
        }

        private void Play()
        {
            modeButtonsParent.gameObject.SetActive(true);
            mainButtonsParent.gameObject.SetActive(false);
        }

        private void Quit()
        {
            EditorApplication.ExitPlaymode();
        }
    }
}
