using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RepeatSequence.UI
{
    public class PausePopupController : PopupController
    {
        [SerializeField] private Button continueButton, restartButton, quitButton;

        public event Action ContinuePressed, RestartPressed, QuitPressed;

        private void Awake() 
        {
            continueButton.onClick.AddListener(OnContinuePressed);
            restartButton.onClick.AddListener(OnRestartPressed);
            quitButton.onClick.AddListener(OnQuitPressed);
        }

        private void OnDestroy() 
        {
            continueButton.onClick.RemoveAllListeners();
            restartButton.onClick.RemoveAllListeners();
            quitButton.onClick.RemoveAllListeners();
        }

        private void OnContinuePressed()
        {
            ContinuePressed?.Invoke();
        }

        private void OnRestartPressed()
        {
            RestartPressed?.Invoke();
        }

        private void OnQuitPressed()
        {
            QuitPressed?.Invoke();
        }
    }
}
