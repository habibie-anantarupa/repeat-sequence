using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace RepeatSequence.Sequence
{
    public class Sequencer : MonoBehaviour
    {
        [SerializeField] private SequenceButton[] sequenceButtons;

        private bool canReceiveInput;
        
        public event Action<ButtonType> SequencePressed;
        public event Action SequenceDone;
        private const float SequenceShowDuration = 3f;

        public SequenceButton[] SequenceButtons { get => sequenceButtons; private set => sequenceButtons = value; }

        private Coroutine showSequenceCoroutine;

        private void Awake() 
        {
            SetupButtons(true);
        }

        private void OnDestroy() 
        {
            SetupButtons(false);
        }

        private void SetupButtons(bool subscribe)
        {
            foreach (SequenceButton button in SequenceButtons)
            {
                if (subscribe)
                {
                    button.onClick.AddListener(() => InputSequence(button.ButtonType));
                }
                else
                {
                    button.onClick.RemoveAllListeners();
                }
                
            }
        }

        private void InputSequence(ButtonType type)
        {
            if (!canReceiveInput) return;
            SequencePressed?.Invoke(type);
        }

        public void ShowSequence(List<SequenceButton> sequence)
        {
            ToggleAllButtons(false);
            if (showSequenceCoroutine != null) StopCoroutine(showSequenceCoroutine);
            showSequenceCoroutine = StartCoroutine(ShowSequenceCoroutine(sequence));
        }

        public void SetCanReceiveInput(bool canReceiveInput)
        {
            this.canReceiveInput = canReceiveInput;
        }

        public void Reset()
        {
            if (showSequenceCoroutine != null) StopCoroutine(showSequenceCoroutine);
            foreach (SequenceButton sequenceButton in sequenceButtons)
            {
                sequenceButton.ResetButton();
            }
        }

        private void ToggleAllButtons(bool interactable)
        {
            foreach (SequenceButton button in SequenceButtons)
            {
                button.interactable = interactable;
            }
        }

        private IEnumerator ShowSequenceCoroutine(List<SequenceButton> sequence)
        {
            float currentDurationBetweenButton = SequenceShowDuration / sequence.Count;

            foreach (SequenceButton sequenceButton in sequence)
            {
                yield return sequenceButton.Blink(currentDurationBetweenButton);
            }

            OnSequenceDone();
        }

        private void OnSequenceDone()
        {
            SequenceDone?.Invoke();
            ToggleAllButtons(true);
        }

        public SequenceButton GetRandomSequenceButton()
        {
            int randomIndex = UnityEngine.Random.Range(0, sequenceButtons.Length);
            return SequenceButtons[randomIndex];
        }
    }
}