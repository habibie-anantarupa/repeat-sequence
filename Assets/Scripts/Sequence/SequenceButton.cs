using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace RepeatSequence.Sequence
{
    public class SequenceButton : Button
    {
        [SerializeField] private ButtonType buttonType;

        private Sprite normalSprite, pressedSprite;
        private TextMeshProUGUI buttonText;
        private Color defaultButtonColor;
        public ButtonType ButtonType { get => buttonType; private set => buttonType = value; }

        protected override void Awake() 
        {
            normalSprite = image.sprite;
            pressedSprite = spriteState.pressedSprite;

            buttonText = GetComponentInChildren<TextMeshProUGUI>();
            defaultButtonColor = buttonText.color;
        }

        public IEnumerator Blink(float duration)
        {
            image.sprite = pressedSprite;
            buttonText.color = Color.white;

            yield return new WaitForSeconds(duration);

            ResetButton();

            yield return new WaitForSeconds(0.1f);
        }

        public void ResetButton()
        {
            image.sprite = normalSprite;
            buttonText.color = defaultButtonColor;
        }
    }

    public enum ButtonType
    {
        Red,
        Green,
        Blue,
        Yellow
    }
}
