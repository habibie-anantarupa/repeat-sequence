using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using RepeatSequence.Controllers;
using RepeatSequence.Sequence;
using RepeatSequence.UI;
using UnityEngine;

namespace RepeatSequence.Game
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private bool hardMode;
        [SerializeField] private int maxSequenceCount = 5;
        [SerializeField] private Sequencer sequencer;
        [SerializeField] private PlayerController playerController;
        [SerializeField] private StartMenuController startMenuController;
        [SerializeField] private GameOverPopupController gameOverPopupController;
        [SerializeField] private PausePopupController pausePopupController;

        private const int DelayBeforeFirstSequence = 1000; // in ms

        private int currentSequenceIndex = 0;
        private int currentSequenceCount = 1;
        private readonly List<SequenceButton> currentSequence = new List<SequenceButton>();

        public event Action<bool> SequencePressed;
        public event Action SequenceFinished;
        public event Action<bool> GameFinished;

        private bool isPaused;
        private bool isInGame;
        private bool win;

        private void Awake()
        {
            sequencer.SequencePressed += OnSequencePressed;
            sequencer.SequenceDone += OnSequenceDone;

            startMenuController.PlayModePressed += OnPlayModePressed;

            gameOverPopupController.PlayAgainPressed += OnRestartPressed;
            gameOverPopupController.QuitPressed += OnQuitPressed;
            
            pausePopupController.ContinuePressed += OnContinuePressed;
            pausePopupController.RestartPressed += OnRestartPressed;
            pausePopupController.QuitPressed += OnQuitPressed;

            startMenuController.Show();
        }

        private void Update() 
        {
            if (isInGame && Input.GetKeyDown(KeyCode.P) && !isPaused)
            {
                Pause();
            }
        }

        private void OnDestroy()
        {
            sequencer.SequencePressed -= OnSequencePressed;
            sequencer.SequenceDone -= OnSequenceDone;

            startMenuController.PlayModePressed -= OnPlayModePressed;

            gameOverPopupController.PlayAgainPressed -= OnRestartPressed;
            gameOverPopupController.QuitPressed -= OnQuitPressed;

            pausePopupController.ContinuePressed -= OnContinuePressed;
            pausePopupController.RestartPressed -= OnRestartPressed;
            pausePopupController.QuitPressed -= OnQuitPressed;
        }

        private void OnPlayModePressed(bool isHardMode)
        {
            hardMode = isHardMode;
            
            StartGame();
        }

        private void OnRestartPressed()
        {
            StartGame();
        }

        private void OnContinuePressed()
        {
            Pause();
        }

        private void OnQuitPressed()
        {
            isInGame = false;
            startMenuController.Show();
        }

        private void ResetGame()
        {
            Time.timeScale = 1f;

            currentSequence.Clear();

            currentSequenceIndex = 0;
            currentSequenceCount = 1;
            playerController.Reset();
            playerController.Show();
            pausePopupController.Hide();
            sequencer.Reset();

            isInGame = true;
            isPaused = false;
            win = false;
        }

        private void StartGame()
        {
            sequencer.SetCanReceiveInput(true);
            ResetGame();
            _ = ShowNextSequence();
        }

        private async Task ShowNextSequence()
        {
            RandomizeSequence();

            await Task.Delay(DelayBeforeFirstSequence);
            
            currentSequenceIndex = 0;
            sequencer.ShowSequence(currentSequence);
        }

        private void RandomizeSequence()
        {
            if (hardMode)
            {
                currentSequence.Clear();
                for (int i = 0; i < currentSequenceCount; i++)
                {
                    currentSequence.Add(sequencer.GetRandomSequenceButton());
                }
            }
            else
            {
                currentSequence.Add(sequencer.GetRandomSequenceButton());
            }
        }

        private void Pause()
        {
            isPaused = !isPaused;
            sequencer.SetCanReceiveInput(!isPaused);

            if (isPaused)
            {
                Time.timeScale = 0f;
                playerController.Hide();
                pausePopupController.Show();
            }
            else
            {
                Time.timeScale = 1f;
                pausePopupController.Hide();
                playerController.Show();
            }
        }

        private void GameOver()
        {
            playerController.Hide();
            gameOverPopupController.Show();
            sequencer.SetCanReceiveInput(false);
            GameFinished?.Invoke(win);
        }

        private void OnSequenceDone()
        {
            sequencer.SetCanReceiveInput(true);
        }

        private void OnSequencePressed(ButtonType type)
        {
            bool isCorrectSequence = currentSequence[currentSequenceIndex].ButtonType == type;
            SequencePressed?.Invoke(isCorrectSequence);

            playerController.ShowResponse(isCorrectSequence);
            if (isCorrectSequence)
            {
                if (currentSequenceIndex < currentSequence.Count - 1)
                {
                    currentSequenceIndex++;
                }
                else
                {
                    OnSequenceFinished();
                }
            }
            else
            {
                if (playerController.Health > 0)
                {
                    sequencer.ShowSequence(currentSequence);
                    currentSequenceIndex = 0;
                }
                else
                {
                    win = false;
                    GameOver();
                }
            }
        }

        private void OnSequenceFinished()
        {
            sequencer.SetCanReceiveInput(false);
            SequenceFinished?.Invoke();

            if (CheckGameOver())
            {
                win = true;
                GameOver();
                return;
            }

            currentSequenceCount++;
            
            _ = ShowNextSequence();
        }

        private bool CheckGameOver()
        {
            return currentSequence.Count == maxSequenceCount;
        }
    }
}
