using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RepeatSequence.Game;

namespace RepeatSequence.Controllers
{
    public class GameAudioController : MonoBehaviour
    {
        [SerializeField] private GameManager gameManager;
        [SerializeField] private AudioData[] audios;

        private AudioSource audioSource;

        private void Awake() 
        {
            audioSource = GetComponent<AudioSource>();
            
            gameManager.SequencePressed += OnSequencePressed;
            gameManager.SequenceFinished += OnSequenceFinished;
            gameManager.GameFinished += OnGameFinished;
        }

        private void OnDestroy() 
        {
            gameManager.SequencePressed -= OnSequencePressed;
            gameManager.SequenceFinished -= OnSequenceFinished;
            gameManager.GameFinished -= OnGameFinished;
        }

        private void OnSequencePressed(bool isCorrectSequence)
        {
            PlayAudio(isCorrectSequence ? AudioType.CorrectSequence : AudioType.IncorrectSequence);
        }

        private void OnSequenceFinished()
        {
            PlayAudio(AudioType.SequenceFinished);
        }

        private void OnGameFinished(bool win)
        {
            PlayAudio(win ? AudioType.GameOverWin : AudioType.GameOverLose);
        }

        private void PlayAudio(AudioType type)
        {
            AudioData audio = null;
            foreach (AudioData audioData in audios)
            {
                if (audioData.type != type) continue;
                audio = audioData;
            }

            if (audio == null || audio.clip == null)
            {
                Debug.Log($"Audio of type {type} does not exist!");
            }
            else
            {
                audioSource.PlayOneShot(audio.clip);
            }
        }
    }

    public enum AudioType
    {
        CorrectSequence,
        IncorrectSequence,
        SequenceFinished,
        GameOverWin,
        GameOverLose,
    }

    [Serializable]
    public class AudioData
    {
        public AudioType type;
        public AudioClip clip;
    }
}
