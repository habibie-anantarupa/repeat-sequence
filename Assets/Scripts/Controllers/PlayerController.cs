using System.Collections;
using RepeatSequence.Sequence;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace RepeatSequence.Controllers
{
    public class PlayerController : MonoBehaviour
    {
        [SerializeField] private Image mascotImage;
        [SerializeField] private TextMeshProUGUI dialogueText;
        [SerializeField] private Image[] healthIndicators;
        [SerializeField] private Sprite correctMascotSprite;
        [SerializeField] private Sprite[] incorrectMascotSprites;
        [SerializeField] private string[] correctDialogueResponses;
        [SerializeField] private string[] incorrectDialogueResponses;

        private const float DialogueShowDuration = 0.5f;
        private const int MaxHealth = 3;
        private Transform dialogueBase;

        private Sprite defaultSprite;
        private Coroutine showDialogueCoroutine;

        public int Health {get; private set;}

        private void Awake() 
        {
            Health = MaxHealth;
            defaultSprite = mascotImage.sprite;
            dialogueBase = dialogueText.transform.parent;
        }

        public void ShowResponse(bool isCorrectSequence)
        {
            if (isCorrectSequence)
            {
                mascotImage.sprite = correctMascotSprite;
                dialogueText.text = correctDialogueResponses[Random.Range(0, correctDialogueResponses.Length)];
            }
            else
            {
                Health--;

                mascotImage.sprite = incorrectMascotSprites[Random.Range(0, incorrectMascotSprites.Length)];
                dialogueText.text = incorrectDialogueResponses[Random.Range(0, incorrectDialogueResponses.Length)];

                SetHealth();
            }

            ShowDialogue();
        }

        private void SetHealth()
        {
            for (int i = healthIndicators.Length - 1; i > 0; i--)
            {
                healthIndicators[i].gameObject.SetActive(i < Health);
            }
        }

        private void ShowDialogue()
        {
            HideDialogue();
            if (showDialogueCoroutine != null) StopCoroutine(showDialogueCoroutine);
            showDialogueCoroutine = StartCoroutine(ShowDialogueCoroutine());
        }

        private IEnumerator ShowDialogueCoroutine()
        {
            dialogueBase.gameObject.SetActive(true);

            yield return new WaitForSeconds(DialogueShowDuration);

            HideDialogue();
        }

        private void HideDialogue()
        {
            dialogueBase.gameObject.SetActive(false);
        }

        public void Reset()
        {
            if (showDialogueCoroutine != null) StopCoroutine(showDialogueCoroutine);
            HideDialogue();
            Health = MaxHealth;
            mascotImage.sprite = defaultSprite;
            SetHealth();
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}
