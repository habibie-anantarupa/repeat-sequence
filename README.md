# Repeat Sequence

## Description
Simple repeat sequence game with two modes:
- Normal\
Each sequence is previous sequence with additinal random sequence
- Hard\
Each sequence is random

## How To Play
First the game will simulate the sequence, then you press the buttons based on the sequence.
Finishing current sequence will make the game continue to the next sequence up until 5 sequence is finished. Finishing all 5 sequences will result in game over with win condition.\
Press P to pause the game.

### Health
Inputting 3 times of incorrect sequence will result in game over with lose condition.
